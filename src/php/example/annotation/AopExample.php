<?php

namespace example\annotation;

use restphp\aop\AopParam;
use restphp\aop\Aspect;

/**
 * @Aspect(name="AopExample")
 */
class AopExample implements Aspect
{
    /**
     * @param AopParam $aopParam
     * @return void
     */
    function before($aopParam)
    {
        // TODO: Implement before() method.
        echo "aop before executed with params:\n ";
        var_dump($aopParam->getProcessParam());
    }

    /**
     * @param AopParam $aopParam
     * @return void
     */
    function after($aopParam)
    {
        // TODO: Implement after() method.
    }

}