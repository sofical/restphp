<?php
namespace example\controller;

use restphp\http\RestHttpResponse;

#RequestMapping(value="")
class ControllerEmptyTest{
    #RequestMapping(value="", method="GET")
    public function index() {
        RestHttpResponse::html('欢迎使用 RestPHP ' . REST_PHP_VERSION . '!');
    }
}