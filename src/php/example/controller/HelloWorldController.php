<?php
namespace example\controller;
use packagetest\PackageTest;
use restphp\http\RestHttpRequest;
use restphp\http\RestHttpResponse;

/**
 * Created by zj.
 * User: zj
 * Date: 2019/6/18 0018
 * Time: 下午 1:44
 */
class HelloWorldController {
    /**
     * get router example.
     */
    #RequestMapping(value="/hello", method="GET")
    public function helloGET() {
        RestHttpResponse::html("Hello world!");
    }

    /**
     * post example.
     */
    #RequestMapping(value="/hello", method="POST")
    public function helloPOST() {
        RestHttpResponse::json(array(
            "data" => "Hello world!"
        ));
    }

    /**
     * Path parameter test.
     */
    #RequestMapping(value="/hello/$more/hh/$more2", method="GET")
    public function paramTest() {
        $param1 = RestHttpRequest::getPathValue("more");
        $param2 = RestHttpRequest::getPathValue("more2");
        RestHttpResponse::html("path parameter 1: {$param1} <br /> path parameter 2: {$param2}");
    }

    /**
     * third package import example.
     */
    #RequestMapping(value=["/package"])
    public function thirdPackageImportTest() {
        $info = PackageTest::run();
        RestHttpResponse::html($info);
    }
}