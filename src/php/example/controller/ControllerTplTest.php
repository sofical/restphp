<?php

namespace example\controller;

use restphp\tpl\RestTpl;

/**
 * @RequestMapping(value="/tpl")
 */
class ControllerTplTest {
    private $_tpl;
    public function __construct() {
        $GLOBALS['_EVN_PARAM']['TPL_DIR'] = PROJECT_ROOT . 'src/resources/template/default';
        $GLOBALS['_EVN_PARAM']['TPL_CACHE_DIR'] = PROJECT_ROOT . 'runtime/template';
        $this->_tpl = new RestTpl();
        $this->_tpl->caching = false;
    }

    /**
     * @Get (value = "")
     * @AopExample(param1="test",param2="test2")
     * @return void
     */
    public function index() {
        $this->_tpl->assign('app_name', 'RESTPHP 3.0');
        $this->_tpl->display('index.tpl');
    }
}