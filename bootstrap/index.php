<?php
/**
 * 程序入口
 * @author sofical
 * @date 2017-03-17
 */
define('PROJECT_ROOT', '../');

// 自动加载composer包
require(PROJECT_ROOT . 'vendor/autoload.php');

//引入RESTPHP配置
require(PROJECT_ROOT . 'config/rest.config.php');

//引入项目配置
require(PROJECT_ROOT . 'config/proj.config.php');

//引入框架
require(DIR_RESTPHP . '/Rest.php');

\restphp\Rest::run();