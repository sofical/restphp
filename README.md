欢迎了解RestPHP,当前版本为3.1.0。

#### RestPHP的特点
支持路径参数，如：`/users/{userId}` 或 `/users/{userId}/orders/{orderId}`

支持AOP，通过使用@Aspect，可以快速实现切面逻辑开发

支持各种HTTP Method，支持Form表单、json、Xml的报文请求处理和响应

支持多语言设置

支持表单注解验证

#### 安装教程

1.  git clone本项目到本地，使用：composer update 安装依赖包

2. 将运行容器根目录配置到程序入口文件所在目录，如：bootstrap。配置URL重新规则，将所有请求地址重写到第一步的程序入口文件。如，Nginx重写配置：
```nginx
location / {
    index  index.php;
    if (!-e $request_filename) {            
        rewrite ^/(.*)$ /index.php?$1 last;                
    }            
}
```

3.  项目根目录下，运行以下命令构建路由文件：php BUILD

4.  访问：http://localhost 查看效果


#### 使用说明

##### 文件加载

文件支持自动加载，不需要在逻辑代码中使用require或include。其中自动加载的区域分为了两块区域。

1、项目代码区，即 DIR_BUILD 下的项目文件。

2、lib区，即DIR_LIB下的文件。lib区一般用于第三方引用插件代码。

文件加载机制是通过命名空间和类名进行自动查询匹配，因此 **类名需要和文件名保持一致** 


##### 路由编写
使用注解@RequestMapping，参数：value、method

现在也可以使用动作关键词注解，如:@Get,@Post,@Put,@Delete,@Patch，不需要method参数。

 **value**   在class和function中都有效，值可以是一个或多，一个时，可以直接写为：value="/index.html"。多个时，应该写为：value=["/", "/index.html", "/index"]

 **method**   HTTP谓词（方法），即：GET、POST、PUT、DELETE、PATCH等，不区分大小写，建议使用大写。

 完整示例：

```php
/**
 * 首页路由.
 * @RequestMapping("")
 */
class IndexController {
    /**
     * 首页.
     * @RequestMapping(value=["/", "/index.html", "/index"], method="GET")
     */
    public function index() {
        // put your logic here
        echo "hello moto!";
    }
}
```


##### 常用请求参数获取
###### 路径参数

路径参数使用RestHttpRequest::getPathValue()方法获取。注：多路由不支持路径参数。

访问路径`/users/97`，获取用户ID：`97`
```php
/**
 * @RequestMapping("/users")
 */
class UserController {
    /**
     * @RequestMapping(value="/{userId}", method="GET")
     */
    public function userInfo() {
        $userId = RestHttpRequest::getGet("userId");
        echo $userId;
    }
}
```


###### Query参数

Query参数使用RestHttpRequest::getGet() 或 RestHttpRequest::getParameterAsObject() 获取；使用 RestHttpRequest::getPageParam() 获取分页对象。

其中RestHttpRequest::getGet() 用于获取单个路径参数，RestHttpRequest::getParameterAsObject() 用于以对象的形式获取一个或多个参数。

**1.RestHttpRequest::getGet()应用举例**

访问路径`/users?name=张`，获取name参数值：

```php
/**
 * @RequestMapping("/users")
 */
class UserController {
    /**
     * @RequestMapping(value="", method="GET")
     */
    public function userList() {
        $name = RestHttpRequest::getGet("name");
        echo $name;
    }
}
```
**2.RestHttpRequest::getParameterAsObject()应用举例**

访问路径`/users?name=张&mobile=1360000`，获取name和mobile的查询对象

定义接收对象，QueryForm:
```php
final class QueryForm {
    private $_name;
    private $_mobile;
    public function getName() {
        return $this->_name;
    }
    public function setName($name) {
        $this->_name = $name;
    }
    public function getMobile() {
        return $this->_mobile;
    }
    public function setMobile($mobile) {
        $this->_mobile = $mobile;
    }
}
```
在UserController中，使用RestHttpRequest::getParameterAsObject()方法获取QueryForm对象：
```php
/**
 * @RequestMapping("/users")
 */
class UserController {
    /**
     * @RequestMapping(value="", method="GET")
     */
    public function userList() {
        $queryForm = RestHttpRequest::getParameterAsObject(new QueryForm());
        var_export($queryForm);
    }
}

```

###### Body参数
Body 参数使用 RestHttpRequest::getBody() 获取。

应用举例，获取body内容：`{"username":"小红","mobile":"13800000000","age":"18"}`

定义接收对象，UserForm:
```php
final class UserForm {
    private $_name;
    private $_mobile;
    private $_age;
    public function getName() {
        return $this->_name;
    }
    public function setName($name) {
        $this-_name = $name;
    }
    public function getMobile() {
        return $this->_mobile;
    }
    public function setMobile($mobile) {
        $this-_mobile = $mobile;
    }
    public function getAge() {
        return $this->_age;
    }
    public function setAge($age) {
        $this->_age = $age;
    }
}
```
在UserController中，使用RestHttpRequest::getBody()方法获取UserForm对象，或者获取为数组：
```php
/**
 * @RequestMapping("/users")
 */
class UserController {
    /**
     * @RequestMapping(value="", method="POST")
     */
    public function newUser() {
        //获取为对象
        $userForm = RestHttpRequest::getBody(new UserForm());
        var_export($userForm);
        //获取为数组
        $arrUser = RestHttpRequest::getBody();
        var_dump($arrUser);
    }
}
```

##### 数据验证
框架提供了以下注解表单验：

@length(min=最小长度,max=最大长度,message=错误提示)

@notnull(message=错误提示)

@mobile(message=错误提示)

@email(message=错误提示)

@domain(message=错误提示)

@date(format=日期格式,message=错误提示)

@range(min=最小长度,max=最大长度,message=错误提示)

@int(min=最小长度,max=最大长度,message=错误提示)

@ipv4(message=错误提示)

@ipv6(message=错误提示)

@inArray(value=[可选值1|可选值2],message=错误提示)

@notEmpty(message=错误提示)

@customer(method=自定义校验方法,message=错误提示)

使用示例：

创建MessageVo:
```php
<?php
namespace classes\controller\api\vo;

/**
 * Class MessageVo
 * @package classes\controller\api\vo
 */
class MessageVo {
    /**
     * 客户名称.
     * @length(min=1,max=20,message=名字输入长度为1~20个字符)
     * @var string.
     */
    private $_name;

    /**
     * 感兴趣的产品.
     * @length(min=1,max=50,message=感兴趣的产品输入长度为1-50个字符)
     * @var string
     */
    private $_product;

    /**
     * 手机号码.
     * @mobile(message=手机号不正确)
     * @var string
     */
    private $_mobile;

    /**
     * 更多说明
     * @length(max=255,message=更多说明长度不能超过255字)
     * @var string
     */
    private $_more;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * @param string $product
     */
    public function setProduct($product)
    {
        $this->_product = $product;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->_mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->_mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getMore()
    {
        return $this->_more;
    }

    /**
     * @param string $more
     */
    public function setMore($more)
    {
        $this->_more = $more;
    }


}
```
在MessagesController使用：
```php
<?php
namespace classes\controller\api;

use classes\controller\api\vo\MessageVo;
use classes\service\CrmMessageService;
use restphp\http\RestHttpRequest;
use restphp\validate\RestValidate;

/**
 * Class MessagesController
 * @RequestMapping(value="/api/messages")
 * @package classes\controller\api
 */
class MessagesController {
    /**
     * 接收消息.
     * @RequestMapping(value="", method="POST")
     * @throws \ReflectionException
     */
    public function receiveMessage() {
        $message = RestHttpRequest::getRequestBody(new MessageVo(), true);

        CrmMessageService::saveMessage($message);
    }
}
```


##### 数据响应

数据响应为开放自由式响应，无特殊固定规则和格式。

框架的RestHttpResponse类提供常用数据响应方法封状；RestTpl类提供了简单的模板引擎。

##### AOP快速上手

自定义一个切面逻辑片段：\example\annotation\AopExample

```php
namespace example\annotation;

use restphp\aop\AopParam;
use restphp\aop\Aspect;

/**
* @Aspect(name="AopExample")
*/
class AopExample implements Aspect
{
    /**
    * @param AopParam $aopParam
    * @return void
    */
    function before($aopParam)
    {
        // TODO: Implement before() method.
        echo "aop before executed with params:\n ";
        var_dump($aopParam->getProcessParam());
    }

    /**
    * @param AopParam $aopParam
    * @return void
    */
    function after($aopParam)
    {
        // TODO: Implement after() method.
    }

}
```

使用AOP（需构建后运行引用代码查看效果）:
```php
<?php
namespace example\controller;

use restphp\tpl\RestTpl;

/**
* @RequestMapping(value="/tpl")
*/
class ControllerTplTest {
    private $_tpl;
    public function __construct() {
        $GLOBALS['_EVN_PARAM']['TPL_DIR'] = PROJECT_ROOT . 'src/resources/template/default';
        $GLOBALS['_EVN_PARAM']['TPL_CACHE_DIR'] = PROJECT_ROOT . 'runtime/template';
        $this->_tpl = new RestTpl();
        $this->_tpl->caching = false;
    }

    /**
    * @Get (value = "")
    * @AopExample(param1="test",param2="test2")
    * @return void
    */
    public function index() {
        $this->_tpl->assign('app_name', 'RESTPHP 3.0');
        $this->_tpl->display('index.tpl');
    }
}
```

官网文档：https://www.restphp.com