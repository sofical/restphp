<?php
//RESTPHP 相关配置
define('REST_PHP_VERSION', '3.0');
define('DIR_LIB', PROJECT_ROOT . 'lib');
define('DIR_RESTPHP', PROJECT_ROOT . 'vendor/sofical/restphp-core/src');
define('DIR_BUILD', PROJECT_ROOT . 'src/php');
define('DIR_BUILD_TARGET', PROJECT_ROOT . 'runtime/target');
define('HTTP_VERSION', '1.1');
define('CONTENT_TYPE', 'application/json');
define('SYS_TIME', time());
define('SYS_MICRO_TIME', microtime(true));