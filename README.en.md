Welcome to RestPHP, the current version is 3.1.0.

#### RestPHP features
Support path parameters, such as: `/users/{userId}` or `/users/{userId}/orders/{orderId}`

Support AOP, by using @ Aspect, it is possible to quickly develop slicing logic

Supports various HTTP methods, handling and responding to message requests for Form forms, JSON, and XML

Support multilingual settings

Support form annotation verification

#### Installation instructions

1. Clone this project to your local, and use: composer update to install dependencies.

2. Configure the running container root directory to the program entry file location, such as: bootstrap. Configure URL rewrite rules to redirect all requests to the first program entry file. For example, Nginx rewrite configuration:
```nginx
location / {
    index  index.php;
    if (!-e $request_filename) {            
        rewrite ^/(.*)$ /index.php?$1 last;                
    }
}
```

3. Run the following command to build the routing file: php BUILD

4. Visit http://localhost to see the effect


#### Use instructions
##### Php file loading
File loading is automatic, no need to use require or include in the logical code. There are two areas of file loading, one is the project code area, and the other is the lib area, which is generally used for third-party plugin code.

File loading mechanism is through the use of namespaces and class names to automatically query match, so **the class name needs to be consistent with the file name**

##### Router
Use @RequestMapping, parameters: value, method

Now action keyword annotations can also be used, such as @ Get, @ Post, @ Put, @ Delete, @ Patch, without the need for method parameters.

**value**   In class and function, both valid, the value can be one or more, one when directly writing as: value="/index.html"。More than one when writing as: value=["/", "/index.html", "/index"]

**method**   HTTP verb (method), such as: GET, POST, PUT, DELETE, PATCH, not case sensitive, recommended to use uppercase.

example:
```php
/**
 * Home page route.
 * @RequestMapping("")
 */
class IndexController {
    /**
     * home page.
     * @RequestMapping(value=["/", "/index.html", "/index"], method="GET")
     */
    public function index() {
        // put your logic here
        echo "hello moto!";
    }
}
```

#### Retrieve commonly used request parameters
##### Path parameters
The path parameter is obtained using the RestHttpRequest:: getPathValue() method. Note: Multi routing does not support path parameters.

Access path `/users/97`, obtain user ID: `97`
```php
/**
 * @RequestMapping("/users")
 */
class UserController {
    /**
     * @RequestMapping(value="/{userId}", method="GET")
     */
    public function userInfo() {
        $userId = RestHttpRequest::getGet("userId");
        echo $userId;
    }
}
```

##### Query string parameters
Use RestHttpRequest:: getGet() or RestHttpRequest:: getParameterAsObject () to obtain the Query parameter; Use RestHttpRequest:: getPageParam() to retrieve the paging object.

RestHttpRequest:: getGet() is used to obtain a single path parameter, while RestHttpRequest:: getParameterAsObject is used to obtain one or more parameters in the form of an object.

**1. RestHttpRequest:: getGet() Application Example**

Access `path/users?Name=Zhang`, get the value of the name parameter:
```php
/**
 * @RequestMapping("/users")
 */
class UserController {
    /**
     * @RequestMapping(value="", method="GET")
     */
    public function userList() {
        $name = RestHttpRequest::getGet("name");
        echo $name;
    }
}
```
**2. RestHttpRequest:: getParameterAsObject() Application Example**

Access path `/users?Name=Zhang&mobile=1360000`, retrieve the query objects for name and mobile

Define the recipient object, QueryForm:
```php
final class QueryForm {
    private $_name;
    private $_mobile;
    public function getName() {
        return $this->_name;
    }
    public function setName($name) {
        $this->_name = $name;
    }
    public function getMobile() {
        return $this->_mobile;
    }
    public function setMobile($mobile) {
        $this->_mobile = $mobile;
    }
}
```
In UserController, use the RestHttpRequest:: getParameterAsObject () method to obtain a QueryForm object:
```php
/**
 * @RequestMapping("/users")
 */
class UserController {
    /**
     * @RequestMapping(value="", method="GET")
     */
    public function userList() {
        $queryForm = RestHttpRequest::getParameterAsObject(new QueryForm());
        var_export($queryForm);
    }
}
```

##### Body parameters
The Body parameter is obtained using RestHttpRequest:: getBody().

For example, to obtain the body content: `{"username": "Xiaohong", "mobile": "13800000000", "age": "18"}`

Define the recipient object, UserForm:
```php
final class UserForm {
    private $_name;
    private $_mobile;
    private $_age;
    public function getName() {
        return $this->_name;
    }
    public function setName($name) {
        $this-_name = $name;
    }
    public function getMobile() {
        return $this->_mobile;
    }
    public function setMobile($mobile) {
        $this-_mobile = $mobile;
    }
    public function getAge() {
        return $this->_age;
    }
    public function setAge($age) {
        $this->_age = $age;
    }
}
```
In UserController, use the RestHttpRequest:: getBody() method to retrieve the UserForm object, or retrieve it as an array:
```php
/**
 * @RequestMapping("/users")
 */
class UserController {
    /**
     * @RequestMapping(value="", method="POST")
     */
    public function newUser() {
        //获取为对象
        $userForm = RestHttpRequest::getBody(new UserForm());
        var_export($userForm);
        //获取为数组
        $arrUser = RestHttpRequest::getBody();
        var_dump($arrUser);
    }
}
```

#### Data validation
The framework provides the following annotation forms for verification:

@Length (min=minimum length, max=maximum length, message=error message)

@Notnull (message=error message)

@Mobile (message=error message)

@Email (message=error message)

@Domain (message=error message)

@Date (format=date format, message=error message)

@Range (min=minimum length, max=maximum length, message=error message)

@Int (min=minimum length, max=maximum length, message=error message)

@IPv4 (message=error message)

@IPv6 (message=error message)

@InArray (value=[optional value 1 | optional value 2], message=error message)

@NotEmpty (message=error message)

@Customer (method=custom verification method, message=error prompt)

Usage example:

Create the MessageVo:
```php
<?php
namespace classes\controller\api\vo;

/**
 * Class MessageVo
 * @package classes\controller\api\vo
 */
class MessageVo {
    /**
     * 客户名称.
     * @length(min=1,max=20,message=名字输入长度为1~20个字符)
     * @var string.
     */
    private $_name;

    /**
     * 感兴趣的产品.
     * @length(min=1,max=50,message=感兴趣的产品输入长度为1-50个字符)
     * @var string
     */
    private $_product;

    /**
     * 手机号码.
     * @mobile(message=手机号不正确)
     * @var string
     */
    private $_mobile;

    /**
     * 更多说明
     * @length(max=255,message=更多说明长度不能超过255字)
     * @var string
     */
    private $_more;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return string
     */
    public function getProduct()
    {
        return $this->_product;
    }

    /**
     * @param string $product
     */
    public function setProduct($product)
    {
        $this->_product = $product;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->_mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->_mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getMore()
    {
        return $this->_more;
    }

    /**
     * @param string $more
     */
    public function setMore($more)
    {
        $this->_more = $more;
    }


}
```
Using in the MessagesController:
```php
<?php
namespace classes\controller\api;

use classes\controller\api\vo\MessageVo;
use classes\service\CrmMessageService;
use restphp\http\RestHttpRequest;
use restphp\validate\RestValidate;

/**
 * Class MessagesController
 * @RequestMapping(value="/api/messages")
 * @package classes\controller\api
 */
class MessagesController {
    /**
     * 接收消息.
     * @RequestMapping(value="", method="POST")
     * @throws \ReflectionException
     */
    public function receiveMessage() {
        $message = RestHttpRequest::getRequestBody(new MessageVo(), true);

        CrmMessageService::saveMessage($message);
    }
}
```

#### Data response

The data response is an open and free-form response with no special fixed rules or formats.

The RestHttpResponse class of the framework provides commonly used data response method envelopes; The RestTpl class provides a simple template engine.

#### AOP Quick Get Started

Customize a slice logic: \example\annotation\OpExample
```php
namespace example\annotation;

use restphp\aop\AopParam;
use restphp\aop\Aspect;

/**
* @Aspect(name="AopExample")
*/
class AopExample implements Aspect
{
    /**
    * @param AopParam $aopParam
    * @return void
    */
    function before($aopParam)
    {
        // TODO: Implement before() method.
        echo "aop before executed with params:\n ";
        var_dump($aopParam->getProcessParam());
    }

    /**
    * @param AopParam $aopParam
    * @return void
    */
    function after($aopParam)
    {
        // TODO: Implement after() method.
    }

}
```

Using AOP (run reference code after building to see the effect):

```php
<?php
namespace example\controller;

use restphp\tpl\RestTpl;

/**
* @RequestMapping(value="/tpl")
*/
class ControllerTplTest {
    private $_tpl;
    public function __construct() {
        $GLOBALS['_EVN_PARAM']['TPL_DIR'] = PROJECT_ROOT . 'src/resources/template/default';
        $GLOBALS['_EVN_PARAM']['TPL_CACHE_DIR'] = PROJECT_ROOT . 'runtime/template';
        $this->_tpl = new RestTpl();
        $this->_tpl->caching = false;
    }

    /**
    * @Get (value = "")
    * @AopExample(param1="test",param2="test2")
    * @return void
    */
    public function index() {
        $this->_tpl->assign('app_name', 'RESTPHP 3.0');
        $this->_tpl->display('index.tpl');
    }
}
```

Document: https://www.restphp.com